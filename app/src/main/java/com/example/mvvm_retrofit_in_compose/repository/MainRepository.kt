package com.example.mvvm_retrofit_in_compose.repository

import com.example.mvvm_retrofit_in_compose.model.Post
import com.example.mvvm_retrofit_in_compose.retrofit.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
// create by ahmadreza(radmehr) amini 2023/08/30
class MainRepository
@Inject
constructor(private val apiService: ApiService){
    fun getPost(): Flow<List<Post>> = flow {
        emit(apiService.getPosts())
    }.flowOn(Dispatchers.IO)
}
