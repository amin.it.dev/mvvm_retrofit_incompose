package com.example.mvvm_retrofit_in_compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.mvvm_retrofit_in_compose.model.Post
import com.example.mvvm_retrofit_in_compose.retrofit.ApiState
import com.example.mvvm_retrofit_in_compose.ui.theme.MyApplicationTheme
import com.example.mvvm_retrofit_in_compose.viewModel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
// create by ahmadreza(radmehr) amini 2023/08/30
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private val mainViewModel: MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting(mainViewModel)
                }
            }
        }
    }
}

@Composable
fun Greeting(mainViewModel: MainViewModel) {
    when (val result = mainViewModel.response.value) {
        is ApiState.Success -> {
            val res : List<Post> = result.data as List<Post>
         print("Success....."+ res[0].title)
            Text(text = res[0].title.toString())


        }

        is ApiState.Failure -> {
           print("Failed......"+result.msg)
        }

        ApiState.Loading -> {
            print("Loading......")
        }

        ApiState.Empty -> {

        }
    }
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MyApplicationTheme {

    }
}