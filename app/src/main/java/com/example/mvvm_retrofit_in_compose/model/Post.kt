package com.example.mvvm_retrofit_in_compose.model
import com.google.gson.annotations.SerializedName

// create by ahmadreza(radmehr) amini 2023/08/30

data class Post(

	@SerializedName("id")
	val id: Int? = null,

	@SerializedName("title")
	val title: String? = null,

	@SerializedName("body")
	val body: String? = null,

	@SerializedName("userId")
	val userId: Int? = null
)


