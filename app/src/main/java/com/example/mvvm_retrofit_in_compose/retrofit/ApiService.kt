package com.example.mvvm_retrofit_in_compose.retrofit

import com.example.mvvm_retrofit_in_compose.model.Post
import retrofit2.http.GET
// create by ahmadreza(radmehr) amini 2023/08/30
interface ApiService {
    @GET("posts")
    suspend fun getPosts(): List<Post>
}