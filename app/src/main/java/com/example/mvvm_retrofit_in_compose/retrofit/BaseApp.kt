package com.example.mvvm_retrofit_in_compose.retrofit

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
// create by ahmadreza(radmehr) amini 2023/08/30
@HiltAndroidApp
class BaseApp : Application() {
}