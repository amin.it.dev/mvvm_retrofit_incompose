package com.example.mvvm_retrofit_in_compose.retrofit
// create by ahmadreza(radmehr) amini 2023/08/30
sealed class ApiState {
    class Success(val data: Any) : ApiState()
    class Failure(val msg: Throwable) : ApiState()
    object Loading: ApiState()
    object Empty: ApiState()
}
